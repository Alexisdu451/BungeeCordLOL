package fr.alexis.syluriaproxy.handler;

import fr.alexis.syluriaproxy.events.MuteEvents;
import fr.alexis.syluriaproxy.manager.MuteManager;
import fr.alexis.syluriaproxy.utils.PlayerUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by alexis on 10/12/2016.
 */
public class MuteHandler implements Listener {
    @EventHandler
    public void onMute(MuteEvents e) {
        String messages = ("§6Modération §f» §6" + e.getmutedBy() + "§f vient de mute §6" + PlayerUtils.getPlayerName(e.getmuted()) + "§f pour §6" + e.getmuteReason());
        for(ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
            if(p.hasPermission("admin") || p.hasPermission("modo") || p.hasPermission("staff")) {{
                p.sendMessage(messages);
            }
            }
        }
    }

}

