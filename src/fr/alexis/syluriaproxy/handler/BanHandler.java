package fr.alexis.syluriaproxy.handler;

import fr.alexis.syluriaproxy.events.BanEvents;

import fr.alexis.syluriaproxy.manager.BanManager;
import fr.alexis.syluriaproxy.utils.PlayerUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;



/**
 * Created by alexis on 10/12/2016.
 */
public class BanHandler implements Listener {

    @EventHandler
    public void onBan(BanEvents e) {
        String messages = ("§6Modération §f» §6" + e.getBannedBy() + "§f vient de ban §6" + PlayerUtils.getPlayerName(e.getBanned()) + "§f pour §6" + e.getBanReason() + "§f pendant §6" + BanManager.getRemainingBanTime(e.getBanned()));
        for(ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
            if(p.hasPermission("admin") || p.hasPermission("modo") || p.hasPermission("staff")) {{
                    p.sendMessage(messages);
                }
            }
        }
    }

}

