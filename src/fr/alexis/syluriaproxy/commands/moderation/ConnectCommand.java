package fr.alexis.syluriaproxy.commands.moderation;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by alexis on 20/11/2016.
 */
public class ConnectCommand extends Command {
    private Main main;
    public ConnectCommand() {
        super("connect");
    }


    @SuppressWarnings("deprecation")
    @Override

    public void execute(CommandSender sender, String[] arg1) {
        ProxiedPlayer p = (ProxiedPlayer) sender;
        if (sender.hasPermission("admin") || sender.hasPermission("modo") || sender.hasPermission("staff")) {
            // TODO Auto-generated method stub
            if(arg1.length >= 1){
                StringBuilder builder = new StringBuilder();
                for(int i = 0; i < arg1.length; i++){
                }
                ServerInfo si = ProxyServer.getInstance().getServerInfo(arg1[0]);
                if(p.getServer().getAddress() != si.getAddress());
                p.connect(si);
                p.sendMessage("§6ModérationTP §8» §7Connexion en cours sur le " + ChatColor.GOLD + arg1[0]);

            } else {
                p.sendMessage(new TextComponent("§6ModérationTP §8» §7Essaye avec /connect <serveur>."));
            }
        } else {
            p.sendMessage("§cVous n'avez pas la permission !");
        }
    }
}
