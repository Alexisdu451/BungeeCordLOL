package fr.alexis.syluriaproxy.commands.moderation;

import fr.alexis.syluriaproxy.Main;
import fr.alexis.syluriaproxy.manager.BanManager;
import fr.alexis.syluriaproxy.utils.PlayerUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by alexis on 19/11/2016.
 */
public class UnBanCommand extends Command {

    public UnBanCommand(String name) {
        super(name);
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        ProxyServer.getInstance().getScheduler().runAsync(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                if(sender.hasPermission("admin") || sender.hasPermission("modo")) {
                    if(args.length == 1) {
                        String playername = args[0];
                        UUID uuid = PlayerUtils.getUniqueId(playername);
                        if(uuid != null) {
                            if(BanManager.isBanned(uuid)) {
                                sender.sendMessage(Main.PREFIX + Main.getConfigManager().getString("lang.commands.unban.unbanned", "{NAME}~" + playername));
                                BanManager.unban(uuid, sender.getName());
                            } else {
                                sender.sendMessage(Main.PREFIX +  "Ce joueur " + playername + "n'est pas ban !");
                            }
                        } else {
                            sender.sendMessage(Main.PREFIX + "Ce joueur n'existe pas");
                        }
                    } else {
                        sender.sendMessage(Main.PREFIX + "§cUtilisation /unban <pseudo>");
                    }
                } else {
                    sender.sendMessage(Main.PREFIX + "§cVous n'avez pas la permission");
                }
            }
        });
    }

}