package fr.alexis.syluriaproxy.commands.moderation;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by alexis on 20/11/2016.
 */
public class BtpCommand  extends Command {
    private Main main;

    public BtpCommand() {
        super("btp");
    }


    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String[] arg1) {
        ProxiedPlayer p = (ProxiedPlayer) sender;
        if (sender.hasPermission("admin") || sender.hasPermission("modo") || sender.hasPermission("staff")) {
            if (arg1.length >= 1) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < arg1.length; i++) {
                }
                if (ProxyServer.getInstance().getPlayer(arg1[0]) != null) {
                    p.sendMessage(new TextComponent("§6ModérationTP §8» §7Téléportation vers " + ChatColor.GOLD + ProxyServer.getInstance().getPlayer(arg1[0]).getName()));
                    p.connect(ProxyServer.getInstance().getPlayer(arg1[0]).getServer().getInfo());
                }
            } else {
                p.sendMessage(new TextComponent("§6ModérationTP §8» §7Essaye avec /btp <pseudo>."));
            }
        } else {
            p.sendMessage("§cVous n'avez pas la permission !");
        }
    }
}

