package fr.alexis.syluriaproxy.commands.moderation;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by alexis on 20/11/2016.
 */
public class StaffChatCommand extends Command {
    private Main main;

    public StaffChatCommand() {
        super("sc");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (sender.hasPermission("admin") || sender.hasPermission("modo") || sender.hasPermission("staff")) {
            if (args.length < 1) {
                sender.sendMessage("§cUtilisation : /sc <Message>");
            } else {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < args.length; i++)
                    builder.append(args[i]).append(" ");
                String msg = builder.toString().trim();

                for (ProxiedPlayer pls : ProxyServer.getInstance().getPlayers()) {
                    if (pls.hasPermission("admin") || pls.hasPermission("modo") || pls.hasPermission("staff")) {
                        pls.sendMessage("§cDiscussion §7 ■ "  + sender.getName() + " : §f" + msg);


                    }
                }
            }
        } else {
            sender.sendMessage(new TextComponent("§cVous n'avez pas la permission."));
        }
    }

}


