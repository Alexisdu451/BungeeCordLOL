package fr.alexis.syluriaproxy.commands.moderation;

import fr.alexis.syluriaproxy.Main;
import fr.alexis.syluriaproxy.manager.MuteManager;
import fr.alexis.syluriaproxy.utils.PlayerUtils;
import fr.alexis.syluriaproxy.utils.TimeUnit;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by alexis on 10/12/2016.
 */
public class MuteCommand extends Command {


    public MuteCommand(String name) {
        super(name);
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        ProxyServer.getInstance().getScheduler().runAsync(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (sender.hasPermission("admin") || sender.hasPermission("modo")) {
                    if (args.length >= 4) {
                        String playername = args[0];
                        String reason = "";
                        for (int i = 3; i <= args.length - 1; i++) {
                            reason = reason + args[i] + " ";
                        }
                        UUID uuid = PlayerUtils.getUniqueId(playername);
                        if (uuid != null) {
                            if (!MuteManager.ismuted(uuid)) {
                                try {
                                    long seconds = Integer.parseInt(args[1]);
                                    TimeUnit unit = TimeUnit.getByString(args[2]);
                                    if (unit != null) {
                                        seconds = seconds * unit.getSeconds();
                                        MuteManager.mute(uuid, seconds, reason, sender.getName());
                                    } else {
                                        sender.sendMessage("§6Modération §7» §cVeuillez bien préciser le temps");
                                    }
                                } catch (NumberFormatException e) {
                                    sender.sendMessage("§6Modération §7» §cUn erreur est survenue : Le nombre" + args[1] + "n'est pas un nombre !");
                                }
                            } else {
                                sender.sendMessage("§6Modération §7» §6" + playername + "§f est déjà mute");
                            }
                        } else {
                            sender.sendMessage("§6Modération §7» §cLe joueur n'existe pas");
                        }
                    } else {
                        sender.sendMessage("§6Modération §7» §cUtillisation : SOON");
                    }
                } else {
                    sender.sendMessage("§6Modération §7» §cVous n'avez pas la permission !");
                }
            }
        });
    }

}

