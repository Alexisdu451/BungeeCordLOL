package fr.alexis.syluriaproxy.commands.moderation;

import fr.alexis.syluriaproxy.Main;
import fr.alexis.syluriaproxy.manager.BanManager;
import fr.alexis.syluriaproxy.utils.PlayerUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by alexis on 19/11/2016.
 */
public class BanCommand extends Command {

    public BanCommand(String name) {
        super(name);
    }


    @Override
    public void execute(final CommandSender sender, final String[] args) {
        ProxyServer.getInstance().getScheduler().runAsync(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (sender.hasPermission("admin") || sender.hasPermission("modo")) {
                    if (args.length >= 2) {
                        String playername = args[0];
                        String reason = "";
                        for (int i = 1; i <= args.length - 1; i++) {
                            reason = reason + args[i] + " ";
                        }
                        UUID uuid = PlayerUtils.getUniqueId(playername);
                        if (uuid != null) {
                            if (!BanManager.isBanned(uuid)) {
                                BanManager.ban(uuid, -1, reason, sender.getName());
                            } else {
                                sender.sendMessage(Main.PREFIX + "Le joueur " + playername + "est déjà ban");
                            }
                        } else {
                            sender.sendMessage(Main.PREFIX + "Le joueur n'existe pas");
                        }
                    } else {
                        sender.sendMessage(Main.PREFIX + "Erreur d'utilisation : /ban <pseudo> <raison>");
                    }
                } else {
                    sender.sendMessage(Main.PREFIX + "§cVou n'avez pas la permission");
                }


            }

        });
    }


}
