package fr.alexis.syluriaproxy.commands.admin;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by alexis on 20/11/2016.
 */
public class AnnonceCommand extends Command {
    private Main main;

    public AnnonceCommand() {
        super("a");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if(sender.hasPermission("admin")) {
            if(args.length >= 1){
                StringBuilder builder = new StringBuilder();
                for(int i = 0; i < args.length; i++){
                    builder.append(args[i]).append(" ");
                }
                String message = builder.toString().trim();
                for(ProxiedPlayer pls : ProxyServer.getInstance().getPlayers()){
                    pls.sendMessage(new TextComponent("§6Annonce §e: §4" + sender.getName() + " §7» §e" + message));
                }
            } else {
                sender.sendMessage(new TextComponent("§cVous devez spécifier un message."));
            }
        } else {
            sender.sendMessage(new TextComponent("§cVous n'avez pas la permission."));
        }
    }
}
