package fr.alexis.syluriaproxy.commands.utils;

import fr.alexis.syluriaproxy.manager.MsgManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by alexis on 27/11/2016.
 */
public class MsgCommand extends Command {
    public MsgCommand() {
        super("msg");
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
            if (arrstring.length > 1) {
                ProxiedPlayer proxiedPlayer2 = ProxyServer.getInstance().getPlayer(arrstring[0]);
                if (proxiedPlayer2 != null) {
                    String string = "";
                    int n = 1;
                    while (n < arrstring.length) {
                        string = String.valueOf(string) + arrstring[n] + " ";
                        ++n;
                    }
                    MsgManager.msg(proxiedPlayer, proxiedPlayer2, string.trim().replace("&", "\u00a7"));
                } else {
                    proxiedPlayer.sendMessage((BaseComponent) new TextComponent("§cLe proxy ne trouve pas ce joueur : " + arrstring[0]));
                }
            } else {
                proxiedPlayer.sendMessage((BaseComponent) new TextComponent("§6MSG §7» §cUsage: /msg [player] [msg]"));
            }
        }
    }
}

