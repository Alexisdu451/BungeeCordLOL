package fr.alexis.syluriaproxy.commands.utils;

import java.util.HashMap;

import java.util.ArrayList;

import fr.alexis.syluriaproxy.Main;
import fr.alexis.syluriaproxy.manager.PartyManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by alexis on 27/11/2016.
 */
public class PartyCommand extends Command {
    private Main plugin;
    public HashMap<String, PartyManager> lastInvite = new HashMap();
    public ArrayList<String> disabledInvites = new ArrayList();

    public PartyCommand(Main plugin) {
        super("party", null, new String[]{"p"});
        this.plugin = plugin;
    }

    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            TextComponent accept;
            ProxiedPlayer p = (ProxiedPlayer)sender;
            if (args.length == 0 || args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
                Main.sendMessage((CommandSender)p, (Object)ChatColor.YELLOW + "------------ " + (Object)ChatColor.GRAY + "Patry Help" + (Object)ChatColor.YELLOW + " ------------");
                Main.sendMessage((CommandSender)p, "/p create - " + (Object)ChatColor.GRAY + "Créer une party.");
                Main.sendMessage((CommandSender)p, "/p disband - " + (Object)ChatColor.GRAY + "Détruit une party.");
                Main.sendMessage((CommandSender)p, "/p list - " + (Object)ChatColor.GRAY + "Voir la liste des membres de la party");
                Main.sendMessage((CommandSender)p, "/p invite <player> - " + (Object)ChatColor.GRAY + "Invite un joueur à une party");
                Main.sendMessage((CommandSender)p, "/p accept - " + (Object)ChatColor.GRAY + "Accept les demande de party.");
                Main.sendMessage((CommandSender)p, "/p leave - " + (Object)ChatColor.GRAY + "Leave la partie.");
                Main.sendMessage((CommandSender)p, "/p kick <player> - " + (Object)ChatColor.GRAY + "Kick un joueur de votre parti.");
                Main.sendMessage((CommandSender)p, "/p setleader <player> - " + (Object)ChatColor.GRAY + "add un joueur leader de la parti");
                Main.sendMessage((CommandSender)p, "/p chat - " + (Object)ChatColor.GRAY + "Envoie un message à toutes la partie.");
                Main.sendMessage((CommandSender)p, "/p toggleinvites - " + (Object)ChatColor.GRAY + " invitations on/off.");
                if (Main.WARP_ENABLED) {
                    Main.sendMessage((CommandSender)p, "/p warp - " + (Object)ChatColor.GRAY + "Teleport tout les membres de la party.");
                }
                return;
            }
            PartyManager party = this.plugin.getParty(p);
            if (args[0].equalsIgnoreCase("create")) {
                if (party != null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous êtes déjà en party !");
                } else {
                    this.plugin.createParty(p);
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous venez de crée une party !");
                }
            } else if (args[0].equalsIgnoreCase("disband")) {
                if (party == null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas dans une party !");
                } else if (!party.isOwner(p)) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas leader de la party !");
                } else {
                    this.plugin.disbandParty(party);
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                if (party == null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas dans une party !");
                } else {
                    Main.sendMessage((CommandSender)p, "§6PARTY LIST :");
                    for (String member : party.getMembers()) {
                        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(member);
                        String msg = "";
                        msg = player != null ? msg + (Object)ChatColor.GREEN + member + (Object)ChatColor.GRAY + " | " + (Object)ChatColor.AQUA + player.getServer().getInfo().getName() : msg + (Object)ChatColor.RED + member;
                        Main.sendMessage((CommandSender)p, msg);
                    }
                }
            } else if (args[0].equalsIgnoreCase("invite")) {
                if (party == null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas dans une party !");
                } else if (!party.isOwner(p)) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas leader de la party !");
                } else if (args.length < 2) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fIl faut specifié le nom du joueur");
                } else {
                    ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[1]);
                    if (player == null) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §f " + player.getName() + " n'est pas en ligne !");
                    } else if (p.getName().equals(player.getName())) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §fSOON");
                    } else if (this.disabledInvites.contains(player.getName())) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» Le joueur à désactivé ses demande de party !");
                    } else {
                        this.lastInvite.put(player.getName(), party);
                        Main.sendMessage((CommandSender)p, "§6Party §7» §fVous venez d'inviter " + player.getName() + " dans votre party !");
                        Main.sendMessage((CommandSender)player, "§6Party §7» §fVous venez de recevoir une demande de party de " + p.getName());
                    }
                }
            } else if (args[0].equalsIgnoreCase("accept")) {
                if (party != null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous êtes déjà en party !");
                } else if (!this.lastInvite.containsKey(p.getName())) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'avez pas d'invitation !");
                } else if (!this.plugin.getParties().contains(this.lastInvite.get(p.getName()))) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fLOL !");
                } else {
                    this.lastInvite.get(p.getName()).addPlayer(p);
                }
            } else if (args[0].equalsIgnoreCase("leave")) {
                if (party == null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas dans une party !");
                } else if (party.getMembers().length <= 1) {
                    this.plugin.disbandParty(party);
                } else if (party.isOwner(p)) {
                    party.removePlayer(p);
                    party.setOwner(party.getRandomMember());
                } else {
                    party.removePlayer(p);
                }
            } else if (args[0].equalsIgnoreCase("kick")) {
                if (party == null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas dans une party !");
                } else if (!party.isOwner(p)) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas leader de cette party !");
                } else if (args.length < 2) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fIl faut specifier le nom du joueur !");
                } else {
                    ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[1]);
                    if (player == null) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §f " + player.getName() + " n'est pas en ligne !");
                    } else if (player.getName().equalsIgnoreCase(p.getName())) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §fKICK");
                    } else if (!party.inParty(player)) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §f " + player.getName() + " n'est pas dans votre party !");
                    } else {
                        party.kickPlayer(player);
                    }
                }
            } else if (args[0].equalsIgnoreCase("setleader")) {
                if (party == null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas dans une party !");
                } else if (!party.isOwner(p)) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas leader de cette party !");
                } else if (args.length < 2) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fIl faut specifier le nom du joueur !");
                } else {
                    ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[1]);
                    if (player == null) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §f " + player.getName() + " n'est pas en ligne !");
                    } else if (!party.inParty(player)) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §f " + player.getName() + " n'est pas dans votre party!");
                    } else {
                        party.setOwner(player.getName());
                    }
                }
            } else if (args[0].equalsIgnoreCase("chat")) {
                if (party == null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas dans une party !");
                } else if (args.length < 2) {
                    if (this.plugin.inPartyChat(p)) {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §fTchat désactivé!");
                        this.plugin.setInPartyChat(p, false);
                    } else {
                        Main.sendMessage((CommandSender)p, "§6Party §7» §fTchat activé!");
                        this.plugin.setInPartyChat(p, true);
                    }
                } else {
                    String message = "";
                    for (int x = 1; x < args.length; ++x) {
                        message = message + args[x] + " ";
                    }
                    message = (Object)ChatColor.GREEN + p.getName() + (Object)ChatColor.YELLOW + ": " + message;
                    party.broadcast(message);
                    for (String spy : this.plugin.getSpies()) {
                        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(spy);
                        if (player == null || party.inParty(player)) continue;
                        Main.sendMessage((CommandSender)player, "§6PartySpy §7» " + message);
                    }
                }
            } else if (args[0].equalsIgnoreCase("warp")) {
                if (!Main.WARP_ENABLED) {
                    Main.sendMessage((CommandSender)p, "LOL");
                } else if (party == null) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas dans une party !");
                } else if (!party.isOwner(p)) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVous n'êtes pas leader de cette party !");
                } else {
                    party.broadcast("§6Party §7» §fVous venez d'être téléporté par " + p.getName());
                    for (ProxiedPlayer player : party.getOnlineMembers()) {
                        if (player.getName().equals(p.getName())) continue;
                        player.connect(p.getServer().getInfo());
                    }
                }
            } else if (args[0].equalsIgnoreCase("toggleinvites")) {
                if (this.disabledInvites.contains(p.getName())) {
                    this.disabledInvites.remove(p.getName());
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVos demande sont activé");
                } else {
                    this.disabledInvites.add(p.getName());
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVos demande sont désactivé");
                }
            } else if (args[0].equalsIgnoreCase("spy")) {
                if (this.plugin.isSpy(p)) {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVos demande sont activé");
                    this.plugin.setSpy(p, false);
                } else {
                    Main.sendMessage((CommandSender)p, "§6Party §7» §fVos demande sont activé");
                    this.plugin.setSpy(p, true);
                }
            }
        }
    }
}
