package fr.alexis.syluriaproxy.commands.utils;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by alexis on 20/11/2016.
 */
public class LobbyCommand extends Command {
    @SuppressWarnings("unused")
    private Main main;

    public LobbyCommand(fr.alexis.syluriaproxy.Main main) {
        super("lobby", "", "hub", "h", "l");
        this.main = main;
        // TODO Auto-generated constructor stub
    }


    @Override
    public void execute(CommandSender arg0, String[] arg1) {
        // TODO Auto-generated method stub
        ProxiedPlayer p = (ProxiedPlayer) arg0;
        p.connect(ProxyServer.getInstance().getServerInfo("hub-1"));
        p.sendMessage("§6Lobby §7»§f Vous venez d'être rediriger sur le lobby");
    }

}
