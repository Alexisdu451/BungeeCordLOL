package fr.alexis.syluriaproxy.commands.utils;

import fr.alexis.syluriaproxy.manager.MsgManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by alexis on 27/11/2016.
 */
public class ReplyCommand
        extends Command {
    public ReplyCommand() {
        super("r");
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        if (arrstring.length > 0) {
            ProxiedPlayer proxiedPlayer2 = MsgManager.getConversation(proxiedPlayer);
            if (proxiedPlayer2 != null) {
                String string = "";
                String[] arrstring2 = arrstring;
                int n = arrstring2.length;
                int n2 = 0;
                while (n2 < n) {
                    String string2 = arrstring2[n2];
                    string = String.valueOf(string) + string2 + " ";
                    ++n2;
                }
                MsgManager.msg(proxiedPlayer, proxiedPlayer2, string.trim().replace("&", "\u00a7"));
            } else {
                proxiedPlayer.sendMessage((BaseComponent)new TextComponent("§6MSG §7» §cVous n'avez pas eu de conversation"));
            }
        } else {
            commandSender.sendMessage((BaseComponent)new TextComponent("§6MSG §7» §cUsage: /r [msg]"));
        }
    }
}
