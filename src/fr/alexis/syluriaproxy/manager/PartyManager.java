package fr.alexis.syluriaproxy.manager;

import java.util.ArrayList;
import java.util.Random;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created by alexis on 27/11/2016.
 */
public class PartyManager {
    private String owner;
    private ArrayList<String> members = new ArrayList();

    public PartyManager(ProxiedPlayer player) {
        this.owner = player.getName();
        this.members.add(player.getName());
    }

    public ProxiedPlayer getOwner() {
        return ProxyServer.getInstance().getPlayer(this.owner);
    }

    public boolean isOwner(ProxiedPlayer player) {
        return this.owner.equals(player.getName());
    }

    public boolean isOwner(String name) {
        return this.owner.equalsIgnoreCase(name);
    }

    public void setOwner(String name) {
        if (this.inParty(name)) {
            this.owner = name;
            this.broadcast("§6Party §7» §f" + name + "est devenu chef de la partie");
        }
    }

    public String[] getMembers() {
        return this.members.toArray(new String[this.members.size()]);
    }

    public ProxiedPlayer[] getOnlineMembers() {
        ArrayList<ProxiedPlayer> players = new ArrayList<ProxiedPlayer>();
        for (String name : this.members) {
            players.add(ProxyServer.getInstance().getPlayer(name));
        }
        return players.toArray(new ProxiedPlayer[players.size()]);
    }

    public boolean inParty(ProxiedPlayer p) {
        return this.members.contains(p.getName());
    }

    public boolean inParty(String name) {
        return this.members.contains(name);
    }

    public void addPlayer(ProxiedPlayer p) {
        this.members.add(p.getName());
        this.broadcast("§6Party §7» §f" + p.getName() + "vient de rejoindre la party");
    }

    public void removePlayer(ProxiedPlayer p) {
        this.broadcast("§6Party §7» §f" + p.getName() + "vient d'être remove de la party !");
        this.members.remove(p.getName());
    }

    public void kickPlayer(ProxiedPlayer p) {
        this.broadcast("§6Party §7» §f" + p.getName() + "vient d'être kick de la party !");
        this.members.remove(p.getName());
    }

    public void broadcast(String msg) {
        for (ProxiedPlayer p : this.getOnlineMembers()) {
            Main.sendMessage((CommandSender)p, msg);
        }
    }

    public String getRandomMember() {
        Random rand = new Random();
        return this.members.get(rand.nextInt(this.members.size()));
    }
}


