package fr.alexis.syluriaproxy.manager;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;

/**
 * Created by alexis on 14/12/2016.
 */
public class LobbyManager implements Listener {
    private Main main;
    public ArrayList<ProxiedPlayer> joueur;

    public LobbyManager(fr.alexis.syluriaproxy.Main main) {
        this.main = main;
        joueur = new ArrayList<ProxiedPlayer>();
    }

    public void onServerKickEvent(ServerKickEvent event) {
        event.getPlayer().connect(ProxyServer.getInstance().getServerInfo("hub"));
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void OnConnectEvent(ServerConnectEvent event) {
        ProxiedPlayer player = event.getPlayer();
        if (event.getTarget().getName().equals("hub")) {
            main.getLogger().info(event.getTarget().getName());
            if (Main.getStatut().servstat("hub-1") != 1 && ProxyServer.getInstance().getServerInfo("hub-1").getPlayers().size() < 25) {
                event.setTarget(ProxyServer.getInstance().getServerInfo("hub-1"));
            } else if (Main.getStatut().servstat("hub-2") != 1 && ProxyServer.getInstance().getServers().containsKey("hub-2")) {
                if (ProxyServer.getInstance().getServerInfo("hub-2").getPlayers().size() < 25) {
                    event.setTarget(ProxyServer.getInstance().getServerInfo("hub-2"));
                }
            } else if (Main.getStatut().servstat("hub-3") != 1 && ProxyServer.getInstance().getServers().containsKey("hub-3")) {
                if (ProxyServer.getInstance().getServerInfo("hub-3").getPlayers().size() < 25) {
                    event.setTarget(ProxyServer.getInstance().getServerInfo("hub-3"));
                }
            } else if (Main.getStatut().servstat("hub-4") != 1 && ProxyServer.getInstance().getServers().containsKey("hub-4")) {
                if (ProxyServer.getInstance().getServerInfo("hub-4").getPlayers().size() < 25) {
                    event.setTarget(ProxyServer.getInstance().getServerInfo("hub-4"));
                }
            } else if (Main.getStatut().servstat("hub-5") != 1 && ProxyServer.getInstance().getServers().containsKey("hub-5")) {
                if (ProxyServer.getInstance().getServerInfo("hub-5").getPlayers().size() < 25) {
                    event.setTarget(ProxyServer.getInstance().getServerInfo("hub-5"));
                }
            } else {
                if (Main.getStatut().servstat("hub-1") != 1 && ProxyServer.getInstance().getServerInfo("hub-1").getPlayers().size() < 50) {
                    event.setTarget(ProxyServer.getInstance().getServerInfo("hub-1"));
                } else if (Main.getStatut().servstat("hub-2") != 1 && ProxyServer.getInstance().getServers().containsKey("hub-2")) {
                    if (ProxyServer.getInstance().getServerInfo("hub-2").getPlayers().size() < 50) {
                        event.setTarget(ProxyServer.getInstance().getServerInfo("hub-2"));
                    }
                } else if (Main.getStatut().servstat("hub-3") != 1 && ProxyServer.getInstance().getServers().containsKey("hub-3")) {
                    if (ProxyServer.getInstance().getServerInfo("hub-3").getPlayers().size() < 50) {
                        event.setTarget(ProxyServer.getInstance().getServerInfo("hub-3"));
                    }
                } else if (Main.getStatut().servstat("hub-4") != 1 && ProxyServer.getInstance().getServers().containsKey("hub-4")) {
                    if (ProxyServer.getInstance().getServerInfo("hub-4").getPlayers().size() < 50) {
                        event.setTarget(ProxyServer.getInstance().getServerInfo("hub-4"));
                    }
                } else if (Main.getStatut().servstat("hub-5") != 1 && ProxyServer.getInstance().getServers().containsKey("hub-5")) {
                    if (ProxyServer.getInstance().getServerInfo("hub-5").getPlayers().size() < 50) {
                        event.setTarget(ProxyServer.getInstance().getServerInfo("hub-5"));
                    }
                } else {
                    player.disconnect(ChatColor.RED + "Tous les hub sont full !");
                }
            }
        }
        if (!event.getTarget().getName().equals("hub-1")) {

        }
    }
}

