package fr.alexis.syluriaproxy.manager;

import fr.alexis.syluriaproxy.Main;
import fr.alexis.syluriaproxy.events.BanEvents;
import fr.alexis.syluriaproxy.events.UnBanEvents;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by alexis on 19/11/2016.
 */
public class BanManager {
    public static boolean isBanned(UUID uuid) {
        ResultSet rs = Main.getSQL().getResult("SELECT * FROM api_ban WHERE UUID='" + uuid.toString() + "'");
        try {
            if(rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void unban(UUID uuid, String unbannedBy) {
        ProxyServer.getInstance().getPluginManager().callEvent(new UnBanEvents(uuid, unbannedBy));
        Main.getSQL().update("DELETE FROM api_ban WHERE UUID='" + uuid.toString() + "'");
    }

    public static void ban(UUID uuid, long seconds, String banReason, String bannedBy) {
        if(!isBanned(uuid)) {
            long end = -1L;
            if(seconds > 0) {
                end = System.currentTimeMillis() + (seconds*1000);
            }
            Main.getSQL().update("INSERT INTO api_ban(UUID, BanEnd, BanReason, BannedBy) " +
                    "VALUES('" + uuid.toString() + "', '" + end + "', '" + banReason + "', '" + bannedBy + "')");
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(uuid);
            if(target != null) {
                target.disconnect(getBanKickMessage(uuid));
            }
            ProxyServer.getInstance().getPluginManager().callEvent(new BanEvents(uuid, end, banReason, bannedBy));
        }
    }

    public static long getBanEnd(UUID uuid) {
        long end = 0;
        ResultSet rs = Main.getSQL().getResult("SELECT * FROM api_ban WHERE UUID='" + uuid.toString() + "'");
        try {
            if(rs.next()) {
                end = rs.getLong("BanEnd");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return end;
    }

    public static String getBanReason(UUID uuid) {
        String str = "-";
        ResultSet rs = Main.getSQL().getResult("SELECT * FROM api_ban WHERE UUID='" + uuid.toString() + "'");
        try {
            if(rs.next()) {
                str = rs.getString("BanReason");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getWhoBanned(UUID uuid) {
        String str = "";
        ResultSet rs = Main.getSQL().getResult("SELECT * FROM api_ban WHERE UUID='" + uuid.toString() + "'");
        try {
            if(rs.next()) {
                str = rs.getString("BannedBy");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getRemainingBanTime(UUID uuid) {
        if(isBanned(uuid)) {
            long end = getBanEnd(uuid);
            if(end > 0) {
                long millis = end-System.currentTimeMillis();
                int days = 0;
                int hours = 0;
                int minutes = 0;
                int seconds = 0;
                while(millis >= 1000) {
                    seconds++;
                    millis -= 1000;
                }
                while(seconds >= 60) {
                    minutes++;
                    seconds -= 60;
                }
                while(minutes >= 60) {
                    hours++;
                    minutes -= 60;
                }
                while(hours >= 24) {
                    days++;
                    hours -= 24;
                }
                return ("{DAYS} day(s), {HOURS} hour(s), {MINUTES} min(s) and {SECONDS} sec(s)");
            } else {
                return ("§7DEF");
            }
        }
        return null;
    }

    public static String getBanKickMessage(UUID uuid) {
        String message = ("§cVous êtes banni de PixelGames" + "\n\n" + "§6Pour: §7" + getBanReason(uuid) + "\n" +"§6Par: §7" + getWhoBanned(uuid) + "\n" + "§6Pour une durée de: §7" + getRemainingBanTime(uuid) );
        return message;
    }

}

