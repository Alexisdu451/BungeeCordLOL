package fr.alexis.syluriaproxy.manager;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by alexis on 27/11/2016.
 */
public class MsgManager {
    private static Map<UUID, UUID> conversations = new HashMap<UUID, UUID>();

    public static ProxiedPlayer getConversation(ProxiedPlayer proxiedPlayer) {
        if (!conversations.containsKey(proxiedPlayer.getUniqueId())) {
            return null;
        }
        return ProxyServer.getInstance().getPlayer(conversations.get(proxiedPlayer.getUniqueId()));
    }
    public static void msg(ProxiedPlayer proxiedPlayer, ProxiedPlayer proxiedPlayer2, String string) {
        proxiedPlayer2.sendMessage((BaseComponent)new TextComponent("§7[§6" + proxiedPlayer.getDisplayName() + " §8-> §6Moi§7] §f" + string));
        proxiedPlayer.sendMessage((BaseComponent)new TextComponent("§7[§6Me §8-> §6" + proxiedPlayer2.getDisplayName() + "§7] §f" + string));
        conversations.put(proxiedPlayer.getUniqueId(), proxiedPlayer2.getUniqueId());
        conversations.put(proxiedPlayer2.getUniqueId(), proxiedPlayer.getUniqueId());
    }
}

