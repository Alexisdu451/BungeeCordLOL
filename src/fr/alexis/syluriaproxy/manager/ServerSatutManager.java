package fr.alexis.syluriaproxy.manager;

import fr.alexis.syluriaproxy.Main;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alexis on 14/12/2016.
 */
public class ServerSatutManager {

    private Main main;

    public ServerSatutManager(fr.alexis.syluriaproxy.Main main) {
        this.main = main;
    }

    public int servstat(String server) {
        String strClassName = "com.mysql.jdbc.Driver";
        int retour = 0;
        try {
            Class.forName(strClassName);
        } catch (ClassNotFoundException e1) {

            e1.printStackTrace();
        }
        try {
            String strUrl = "jdbc:mysql://localhost:3306/minecraft";
            java.sql.Connection conn = DriverManager.getConnection(strUrl, main.pseudo, main.mdp);
            String Query = "SELECT * FROM hub_statut WHERE name = '" + server + "';";
            ResultSet rs1 = conn.createStatement().executeQuery(Query);
            while (rs1.next()) {
                if (rs1.getInt("statut") > 0) {
                    retour = rs1.getInt("statut");
                }

            }
            if (retour == 8) {
                retour++;
            }
            conn.close();
        } catch (SQLException e) {
            main.getLogger().info("ERREUR SQL_Bungee_Startup Veuillez contacter un administrateur");
            main.getLogger().info(e.getMessage());
        }

        return retour;
    }

    @SuppressWarnings("static-access")
    public String servtype(String server) {
        String strClassName = "com.mysql.jdbc.Driver";
        String retour = "";
        try {
            Class.forName(strClassName);
        } catch (ClassNotFoundException e1) {

            e1.printStackTrace();
        }
        try {
            String strUrl = "jdbc:mysql://localhost:3306/minecraft";
            java.sql.Connection conn = DriverManager.getConnection(strUrl, main.pseudo, main.mdp);
            String Query = "SELECT * FROM hub_statut WHERE name = '" + server + "';";
            ResultSet rs1 = conn.createStatement().executeQuery(Query);
            while (rs1.next()) {
                if (rs1.getInt("statut") > 0) {
                    retour = rs1.getString("game_type");
                }

            }
            conn.close();
        } catch (SQLException e) {
            main.getLogger().info("ERREUR SQL_Bungee_Startup Veuillez contacter un administrateur");
            main.getLogger().info(e.getMessage());
        }

        return retour;
    }
}
