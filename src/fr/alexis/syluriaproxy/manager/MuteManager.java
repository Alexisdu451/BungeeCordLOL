package fr.alexis.syluriaproxy.manager;

import fr.alexis.syluriaproxy.Main;
import fr.alexis.syluriaproxy.events.MuteEvents;
import fr.alexis.syluriaproxy.events.UnMuteEvents;
import fr.alexis.syluriaproxy.utils.TimeUnit;
import net.md_5.bungee.api.ProxyServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * Created by alexis on 10/12/2016.
 */
public class MuteManager {
    public static boolean ismuted(UUID uuid) {
        ResultSet rs = Main.getSQL().getResult("SELECT * FROM api_mute WHERE UUID='" + uuid.toString() + "'");
        try {
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void unmute(UUID uuid, String unmutedBy) {
        ProxyServer.getInstance().getPluginManager().callEvent(new UnMuteEvents(uuid, unmutedBy));
        Main.getSQL().update("DELETE FROM api_mute WHERE UUID='" + uuid.toString() + "'");
    }

    public static void mute(UUID uuid, long seconds, String MuteReason, String MutedBy) {
        if (!ismuted(uuid)) {
            long end = -1L;
            if (seconds > 0) {
                end = System.currentTimeMillis() + (seconds * 1000);
            }
            ProxyServer.getInstance().getPluginManager().callEvent(new MuteEvents(uuid, end, MuteReason, MutedBy));
            Main.getSQL().update("INSERT INTO api_mute(UUID, MuteEnd, MuteReason, MutedBy) " +
                    "VALUES('" + uuid.toString() + "', '" + end + "', '" + MuteReason + "', '" + MutedBy + "')");
        }
    }

    public static long getMuteEnd(UUID uuid) {
        long end = 0;
        ResultSet rs = Main.getSQL().getResult("SELECT * FROM api_mute WHERE UUID='" + uuid.toString() + "'");
        try {
            if (rs.next()) {
                end = rs.getLong("MuteEnd");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return end;
    }

    public static String getMuteReason(UUID uuid) {
        String str = "";
        ResultSet rs = Main.getSQL().getResult("SELECT * FROM api_mute WHERE UUID='" + uuid.toString() + "'");
        try {
            if (rs.next()) {
                str = rs.getString("MuteReason");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getWhomuted(UUID uuid) {
        String str = "";
        ResultSet rs = Main.getSQL().getResult("SELECT * FROM api_mute WHERE UUID='" + uuid.toString() + "'");
        try {
            if (rs.next()) {
                str = rs.getString("MutedBy");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getRemainingmuteTime(UUID uuid) {
        if (ismuted(uuid)) {
            long end = getMuteEnd(uuid);
            if (end > 0) {
                long millis = end - System.currentTimeMillis();
                int days = 0;
                int hours = 0;
                int minutes = 0;
                int seconds = 0;
                while (millis >= 1000) {
                    seconds++;
                    millis -= 1000;
                }
                while (seconds >= 60) {
                    minutes++;
                    seconds -= 60;
                }
                while (minutes >= 60) {
                    hours++;
                    minutes -= 60;
                }
                while (hours >= 24) {
                    days++;
                    hours -= 24;
                }
                return Main.getConfigManager().timeFormat(days, hours, minutes, seconds);
            } else {
                return ("§7DEF");
            }
        }
        return null;
    }

    public static List<String> lines(UUID uuid) {
        return Main.getConfigManager().getStringList("lang.mutemessage",
                "{REASON}~" + getMuteReason(uuid),
                "{BY}~" + getWhomuted(uuid),
                "{REMAININGTIME}~" + getRemainingmuteTime(uuid));
    }

}




