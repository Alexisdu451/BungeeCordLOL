package fr.alexis.syluriaproxy.events;

import net.md_5.bungee.api.plugin.Event;

import java.util.UUID;

/**
 * Created by alexis on 10/12/2016.
 */
public class UnMuteEvents extends Event {

    private UUID unmuted;
    private String unmutedBy;

    public UnMuteEvents(UUID unmuted, String unmutedBy) {
        this.unmuted = unmuted;
        this.unmutedBy = unmutedBy;
    }

    public String getUnmutedBy() {
        return unmutedBy;
    }

    public UUID getUnmuted() {
        return unmuted;
    }
}
