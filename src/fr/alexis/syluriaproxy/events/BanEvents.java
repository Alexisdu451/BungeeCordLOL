package fr.alexis.syluriaproxy.events;

import java.util.UUID;
import net.md_5.bungee.api.plugin.Event;

/**
 * Created by alexis on 19/11/2016.
 */
public class BanEvents extends Event {

    private UUID banned;
    private long banEnd;
    private String banReason;
    private String bannedBy;

    public BanEvents(UUID banned, long banEnd, String banReason, String bannedBy) {
        this.banned = banned;
        this.banEnd = banEnd;
        this.banReason = banReason;
        this.bannedBy = bannedBy;
    }

    public long getBanEnd() {
        return banEnd;
    }

    public String getBannedBy() {
        return bannedBy;
    }

    public String getBanReason() {
        return banReason;
    }

    public UUID getBanned() {
        return banned;
    }
}

