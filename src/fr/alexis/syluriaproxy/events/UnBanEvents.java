package fr.alexis.syluriaproxy.events;

import net.md_5.bungee.api.plugin.Event;

import java.util.UUID;

/**
 * Created by alexis on 19/11/2016.
 */
public class UnBanEvents extends Event {

    private UUID unbanned;
    private String unbannedBy;

    public UnBanEvents(UUID unbanned, String unbannedBy) {
        this.unbanned = unbanned;
        this.unbannedBy = unbannedBy;
    }

    public String getUnbannedBy() {
        return unbannedBy;
    }

    public UUID getUnbanned() {
        return unbanned;
    }
}