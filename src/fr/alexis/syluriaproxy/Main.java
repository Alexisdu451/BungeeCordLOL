package fr.alexis.syluriaproxy;


import fr.alexis.syluriaproxy.commands.admin.AnnonceCommand;
import fr.alexis.syluriaproxy.commands.moderation.*;
import fr.alexis.syluriaproxy.commands.bloque.BungeeCommand;
import fr.alexis.syluriaproxy.commands.bloque.PlCommand;
import fr.alexis.syluriaproxy.commands.utils.LobbyCommand;
import fr.alexis.syluriaproxy.commands.utils.MsgCommand;
import fr.alexis.syluriaproxy.commands.utils.PartyCommand;
import fr.alexis.syluriaproxy.commands.utils.ReplyCommand;
import fr.alexis.syluriaproxy.handler.BanHandler;
import fr.alexis.syluriaproxy.handler.MuteHandler;
import fr.alexis.syluriaproxy.listener.*;
import fr.alexis.syluriaproxy.manager.*;
import fr.alexis.syluriaproxy.utils.BungeeManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


/**
 * Created by alexis on 17/11/2016. mdp sudo: PbAJzL6z4,8%]6e
 */
public class Main extends Plugin {

    public static String PREFIXS = "§6PixelModération §7»", CONSOLE_PREFIX = "[LOL] ";

    public fr.alexis.syluriaproxy.listener.PingListener pingListener;
    public BungeeManager bungeeManager;
    private static Main instance;
    private static Database sql;
    public static boolean WARP_ENABLED;
    private static ConfigManager configManager;
    private static ServerSatutManager serverSatutManager;
    public static Main plugin;
    public static String PREFIX;
    private ArrayList<PartyManager> parties = new ArrayList();
    private ArrayList<String> partyChat = new ArrayList();
    private ArrayList<String> spies = new ArrayList();
    public String desc = "ala4--- ERREUR ---";
    public String Maintenance_msg = "§cLe serveur est en Maintenance ! :)";
    public int maintenance = 0;
    public int alpha = 0;
    public int beta = 0;
    public int auto_add_max = 0;
    public int max_players = 100;
    public static String pseudo = "devdev";
    public static String mdp = "c7vR3ynYepkTLL7T";


    @Override
    public void onEnable() {
        Main.plugin = this;
        pingListener = new fr.alexis.syluriaproxy.listener.PingListener(this);
        bungeeManager = new fr.alexis.syluriaproxy.utils.BungeeManager(this);
        instance = this;
        WARP_ENABLED = true;
        configManager = new ConfigManager();
        configManager.init();
        log("Connection à la BD.");
        sql.openConnection();
        if (sql.isConnected()) {
            log("Connection ok");
            log("Bungee est load !");
        } else {
            log("An internal error occured whilst connecting to SQL.");
        }
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BanCommand("ban"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new UnBanCommand("unban"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new StaffChatCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new AnnonceCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BtpCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ConnectCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PlCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BungeeCommand());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new BanListeners());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ModListeners(this));
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ConnectListener(this));
        ProxyServer.getInstance().getPluginManager().registerListener(this, new JoinListener(this));
        ProxyServer.getInstance().getPluginManager().registerListener(this, new PingListener(this));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PartyCommand(this));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MsgCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ReplyCommand());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new BanHandler());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new MuteHandler());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MuteCommand("mute"));
        ProxyServer.getInstance().getPluginManager().registerListener(this, new LobbyManager(this));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new LobbyCommand(this));

        scheduler();
    }


    public void log(String str) {
        System.out.println(CONSOLE_PREFIX + str);
    }

    public static Main getInstance() {
        return instance;
    }

    public static Database getSQL() {
        return sql;
    }

    public static void setSQL(Database sql) {
        Main.sql = sql;
    }

    public static ConfigManager getConfigManager() {
        return configManager;
    }

    public static ServerSatutManager getStatut() {
        return serverSatutManager;
    }


    public ArrayList<PartyManager> getParties() {
        return this.parties;
    }

    public PartyManager getParty(ProxiedPlayer p) {
        for (PartyManager party : this.getParties()) {
            if (!party.inParty(p)) continue;
            return party;
        }
        return null;
    }

    public boolean hasParty(ProxiedPlayer p) {
        return this.getParty(p) != null;
    }

    public boolean validParty(PartyManager party) {
        return this.parties.contains(party);
    }

    public boolean inPartyChat(ProxiedPlayer p) {
        return this.partyChat.contains(p.getName());
    }

    public void setInPartyChat(ProxiedPlayer p, boolean value) {
        if (value && !this.inPartyChat(p)) {
            this.partyChat.add(p.getName());
        } else if (!value && this.inPartyChat(p)) {
            this.partyChat.remove(p.getName());
        }
    }

    public boolean isSpy(ProxiedPlayer p) {
        return this.spies.contains(p.getName());
    }

    public void setSpy(ProxiedPlayer p, boolean value) {
        if (value && !this.isSpy(p)) {
            this.spies.add(p.getName());
        } else if (!value && this.isSpy(p)) {
            this.spies.remove(p.getName());
        }
    }

    public String[] getSpies() {
        return this.spies.toArray(new String[this.spies.size()]);
    }

    public PartyManager createParty(ProxiedPlayer p) {
        PartyManager party = new PartyManager(p);
        this.parties.add(party);
        return party;
    }

    public void disbandParty(PartyManager party) {
        this.parties.remove(party);
        party.broadcast("§6Party §7» §fLa partie est disband !");
    }

    public static void sendMessage(CommandSender sender, String msg) {
        if (sender != null)
            sender.sendMessage(TextComponent.fromLegacyText((String) (msg)));
    }

    public void onDisable() {
        getLogger().info("Arrêt du proxy !");
    }


    public void scheduler() {

        ProxyServer.getInstance().getScheduler().schedule(plugin, new Runnable() {

            @Override
            public void run() {
                bungeeManager.startup();
                scheduler();
            }

        }, 30, TimeUnit.SECONDS);
    }


}




    
