package fr.alexis.syluriaproxy.utils;

import fr.alexis.syluriaproxy.Main;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alexis on 26/11/2016.
 */
public class BungeeManager {
    private Main main;

    public BungeeManager(fr.alexis.syluriaproxy.Main main) {
        this.main = main;
        startup();
    }

    public void startup() {
        String strClassName = "com.mysql.jdbc.Driver";
        try {
            Class.forName(strClassName);
        } catch (ClassNotFoundException e1) {

            e1.printStackTrace();
        }
        try {
            String strUrl = "jdbc:mysql://localhost/minecraft";

            @SuppressWarnings("static-access")
            java.sql.Connection conn = DriverManager.getConnection(strUrl,main.pseudo,main.mdp);
            String Query = "SELECT * FROM bungee_config WHERE id = '1';";
            ResultSet rs1 = conn.createStatement().executeQuery(Query);
            while(rs1.next()) {
                if (rs1.getInt("max_players") > 0){
                    main.auto_add_max = rs1.getInt("auto_add_max");
                    main.maintenance = rs1.getInt("maintenance");
                    main.Maintenance_msg = rs1.getString("Maintenance_msg");
                    main.max_players = rs1.getInt("max_players");
                    main.desc = rs1.getString("desription");
                    main.alpha = rs1.getInt("alpha");
                    main.beta = rs1.getInt("beta");
                }

            }
            conn.close();
        } catch(SQLException e) {
            main.getLogger().info("ERREUR SQL_Bungee_Startup Veuillez contacter un administrateur");
            main.getLogger().info(e.getMessage());
        }
    }
}

