package fr.alexis.syluriaproxy.listener;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;

/**
 * Created by alexis on 27/11/2016.
 */
public class ConnectListener implements Listener {
    private Main main;
    public ArrayList<ProxiedPlayer> joueur;

    public ConnectListener(fr.alexis.syluriaproxy.Main main) {
        this.main = main;
        joueur = new ArrayList<ProxiedPlayer>();
    }

    @EventHandler
    public void onConnect(ServerConnectedEvent e) {
        ProxiedPlayer pp = e.getPlayer();
        if (main.hasParty(pp) && main.getParty(pp).isOwner(pp)) {
            for (ProxiedPlayer pp2 : main.getParty(pp).getOnlineMembers()) {
                pp2.connect(e.getServer().getInfo());
                pp2.sendMessage((BaseComponent) new TextComponent("§6Party §7» §fVotre groupe vient de rejoindre un serveur de jeux."));
            }
        }
    }
}


