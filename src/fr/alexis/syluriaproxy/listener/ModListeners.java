package fr.alexis.syluriaproxy.listener;

import fr.alexis.syluriaproxy.Main;
import fr.alexis.syluriaproxy.manager.BanManager;
import fr.alexis.syluriaproxy.manager.MuteManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.List;
import java.util.UUID;

/**
 * Created by alexis on 19/11/2016.
 */
public class ModListeners implements Listener {
    private Main main;

    public ModListeners(fr.alexis.syluriaproxy.Main main) {
        this.main = main;
    }

    @EventHandler
    public void onTabComplete(TabCompleteEvent ev) {
        String partialPlayerName = ev.getCursor().toLowerCase();
        int lastSpaceIndex = partialPlayerName.lastIndexOf(' ');
        if (lastSpaceIndex >= 0) {
            partialPlayerName = partialPlayerName.substring(lastSpaceIndex + 1);
        }
        for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
            if (p.getName().toLowerCase().startsWith(partialPlayerName)) {
                ev.getSuggestions().add(p.getName());
            }
        }
    }

    @EventHandler
    public void onLogin(LoginEvent e) {
        UUID uuid = e.getConnection().getUniqueId();
        if (BanManager.isBanned(uuid)) {
            long end = BanManager.getBanEnd(uuid);
            long current = System.currentTimeMillis();
            if (end > 0) {
                if (end < current) {
                    BanManager.unban(uuid, "automatic");
                } else {
                    e.setCancelled(true);
                    e.setCancelReason(BanManager.getBanKickMessage(uuid));
                }
            } else {
                e.setCancelled(true);
                e.setCancelReason(BanManager.getBanKickMessage(uuid));
            }
        }
    }

    @EventHandler
    public void onChat(ChatEvent e) {
        ProxiedPlayer p = ((ProxiedPlayer) e.getSender());
        String msg = e.getMessage();
        if(MuteManager.ismuted(p.getUniqueId())) {
            long end = MuteManager.getMuteEnd(p.getUniqueId());
            long current = System.currentTimeMillis();
            if(end > 0) {
                if(end > current) {
                    if(msg.startsWith("/")) {
                        List<String> blocked = Main.getConfigManager().getStringList("mute_deny_commands");
                        for(String block : blocked) {
                            if(msg.toLowerCase().startsWith(block.toLowerCase())) {
                                e.setCancelled(true);
                                for(String part : MuteManager.lines(p.getUniqueId())) {
                                    p.sendMessage(Main.PREFIX + part);
                                }
                                return;
                            }
                        }
                    } else {
                        for(String part : MuteManager.lines(p.getUniqueId())) {
                            p.sendMessage(Main.PREFIX + part);
                        }
                        e.setCancelled(true);
                    }
                } else {
                    MuteManager.unmute(p.getUniqueId(), "automatic");
                }
            } else {
                if(msg.startsWith("/")) {
                    List<String> blocked = Main.getConfigManager().getStringList("mute_deny_commands");
                    for(String block : blocked) {
                        if(msg.toLowerCase().startsWith(block.toLowerCase())) {
                            e.setCancelled(true);
                            for(String part : MuteManager.lines(p.getUniqueId())) {
                                p.sendMessage(Main.PREFIX + part);
                            }
                            return;
                        }
                    }
                } else {
                    for(String part : MuteManager.lines(p.getUniqueId())) {
                        p.sendMessage(Main.PREFIX + part);
                    }
                    e.setCancelled(true);
                }
            }
        }
    }

}





