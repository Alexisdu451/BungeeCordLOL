package fr.alexis.syluriaproxy.listener;

import fr.alexis.syluriaproxy.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by alexis on 26/11/2016.
 */
public class PingListener implements Listener {
    private Main main;

    public PingListener(fr.alexis.syluriaproxy.Main main) {
        this.main = main;
    }

    @EventHandler
    public void onPingListener(ProxyPingEvent event) {
        ServerPing r = event.getResponse();
        ServerPing.Players p = r.getPlayers();

        if (main.auto_add_max != 0) {
            if (p.getOnline() + 1 >= main.max_players) {
                main.max_players++;
            }
        }

        p = new ServerPing.Players(main.max_players, p.getOnline(), p.getSample());
        if (main.maintenance != 1) {
            ServerPing rep = new ServerPing(r.getVersion(), p, ChatColor.translateAlternateColorCodes('&', main.desc), r.getFaviconObject());
            event.setResponse(rep);
        } else if (main.maintenance != 0) {
            ServerPing rep1 = new ServerPing(r.getVersion(), p, ChatColor.translateAlternateColorCodes('&', main.Maintenance_msg), r.getFaviconObject());
            event.setResponse(rep1);
        }
    }
}

