package fr.alexis.syluriaproxy.listener;

import fr.alexis.syluriaproxy.Main;
import fr.alexis.syluriaproxy.events.UnBanEvents;
import fr.alexis.syluriaproxy.utils.PlayerUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.List;

/**
 * Created by alexis on 19/11/2016.
 */
public class UnBanListeners implements Listener {

    @EventHandler
    public void onUnban(UnBanEvents e) {
        List<String> messages = Main.getConfigManager().getStringList("lang.commands.unban.broadcast",
                "{BY}~" + e.getUnbannedBy(),
                "{NAME}~" + PlayerUtils.getPlayerName(e.getUnbanned()));
        for(ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
            if(p.hasPermission("admin")) {
                for(String msg : messages) {
                    p.sendMessage(Main.PREFIX + msg);
                }
            }
        }
    }

}
