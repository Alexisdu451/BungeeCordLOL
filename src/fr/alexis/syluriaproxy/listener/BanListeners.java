package fr.alexis.syluriaproxy.listener;

import fr.alexis.syluriaproxy.events.BanEvents;
import fr.alexis.syluriaproxy.manager.BanManager;
import fr.alexis.syluriaproxy.utils.PlayerUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

/**
 * Created by alexis on 19/11/2016.
 */
public class BanListeners implements Listener {

    public void DisconectListener(PlayerDisconnectEvent e, BanEvents e1, final String[] args) {
        String playername = args[0];
        UUID uuid = PlayerUtils.getUniqueId(playername);
        if (uuid != null) {
            if (!BanManager.isBanned(uuid)) {
                e.getPlayer().disconnect(BanManager.getBanKickMessage(uuid));
                    }
                }
            }
        }

